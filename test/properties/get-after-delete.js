const mocha = require('mocha')
const assert = require('chai').assert
const deepMerge = require('deepmerge')
const propertiesHistories2 = require('../fixtures/properties-histories-2.json')

const describe = mocha.describe
const it = mocha.it

const cleanBody = (body) => body.map((chunk) => {
  const newChunk = deepMerge({}, chunk)

  delete newChunk.id
  delete newChunk.createdAt
  delete newChunk.updatedAt

  return newChunk
})

describe('GET /properties && GET /properties/{id}/histories', () => {
  it('should get 1 property', (done) => {
    global.request.get('/properties')
    .end((err, res) => {
      const body = cleanBody(res.body)

      let state = deepMerge({}, propertiesHistories2[2])
      delete state.propertyId
      state = [state]

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual(state, body)

      done()
    })
  })

  it('should return 404 when trying to fetch a deleted property which is now non existing', (done) => {
    global.request.get('/properties/1')
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 404)

      done()
    })
  })

  it('should return 404 when trying to fetch property histories of a deleted property which is now non existing', (done) => {
    global.request.get('/properties/3/histories')
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 404)

      done()
    })
  })

  it('should fetch an existing property', (done) => {
    global.request.get('/properties/2')
    .end((err, res) => {
      delete res.body.id
      delete res.body.createdAt
      delete res.body.updatedAt

      const state = deepMerge({}, propertiesHistories2[2])
      delete state.propertyId

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual(state, res.body)

      done()
    })
  })

  it('should fetch an existing property histories', (done) => {
    global.request.get('/properties/2/histories')
    .end((err, res) => {
      const body = cleanBody(res.body)

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual(propertiesHistories2, body)

      done()
    })
  })
})
