const mocha = require('mocha')
const assert = require('chai').assert
const deepMerge = require('deepmerge')
const propertiesPost = require('../fixtures/properties-post.json')

const describe = mocha.describe
const it = mocha.it

describe('POST /properties', () => {
  it('should insert 3 properties', (done) => {
    global.request.post('/properties')
    .send(propertiesPost[0])
    .end((err, res1) => {
      delete res1.body.id
      delete res1.body.createdAt
      delete res1.body.updatedAt

      assert.isNull(err)
      assert.equal(res1.status, 201)
      assert.deepEqual(propertiesPost[0], res1.body)

      global.request.post('/properties')
      .send(propertiesPost[1])
      .end((err, res2) => {
        delete res2.body.id
        delete res2.body.createdAt
        delete res2.body.updatedAt

        assert.isNull(err)
        assert.equal(res2.status, 201)
        assert.deepEqual(propertiesPost[1], res2.body)

        global.request.post('/properties')
        .send(propertiesPost[2])
        .end((err, res3) => {
          delete res3.body.id
          delete res3.body.createdAt
          delete res3.body.updatedAt

          assert.isNull(err)
          assert.equal(res3.status, 201)
          assert.deepEqual(propertiesPost[2], res3.body)

          done()
        })
      })
    })
  })

  it('should not insert - missing address.line1', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.address.line1

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing address.line4', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.address.line4

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing address.postCode', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.address.postCode

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing address.city', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.address.city

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing address.country', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.address.country

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing owner', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.owner

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing incomeGenerated', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.incomeGenerated

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - invalid range incomeGenerated', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    body.incomeGenerated = 0

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing numberOfBedrooms', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.numberOfBedrooms

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - invalid range numberOfBedrooms', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    body.numberOfBedrooms = -1

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - missing numberOfBathrooms', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    delete body.numberOfBathrooms

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - invalid range numberOfBathrooms', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    body.numberOfBathrooms = 0

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })

  it('should not insert - invalid airbnbId', (done) => {
    const body = deepMerge({}, propertiesPost[0])
    body.airbnbId = 242424242424

    global.request.post('/properties')
    .send(body)
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 422)

      done()
    })
  })
})
