const mocha = require('mocha')
const assert = require('chai').assert
const deepMerge = require('deepmerge')
const propertiesPost = require('../fixtures/properties-post.json')
const propertiesHistories1 = require('../fixtures/properties-histories-1.json')
const propertiesHistories2 = require('../fixtures/properties-histories-2.json')

const describe = mocha.describe
const it = mocha.it

const cleanBody = (body) => body.map((chunk) => {
  const newChunk = deepMerge({}, chunk)

  delete newChunk.id
  delete newChunk.createdAt
  delete newChunk.updatedAt

  return newChunk
})

describe('GET /properties/{id}/histories', () => {
  it('should fetch the histories of property with id 1', (done) => {
    global.request.get('/properties/1/histories')
    .end((err, res) => {
      const body = cleanBody(res.body)

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual(propertiesHistories1, body)

      done()
    })
  })

  it('should fetch the histories of property with id 2', (done) => {
    global.request.get('/properties/2/histories')
    .end((err, res) => {
      const body = cleanBody(res.body)

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual(propertiesHistories2, body)

      done()
    })
  })

  it('should fetch the histories of property with id 3', (done) => {
    global.request.get('/properties/3/histories')
    .end((err, res) => {
      const body = cleanBody(res.body)

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual([{ ...propertiesPost[2], propertyId: 3 }], body)

      done()
    })
  })

  it('should return 404 when trying to fetch a non existing property', (done) => {
    global.request.get('/properties/1337/histories')
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 404)

      done()
    })
  })
})
