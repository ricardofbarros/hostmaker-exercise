const mocha = require('mocha')
const assert = require('chai').assert
const deepMerge = require('deepmerge')
const propertiesPost = require('../fixtures/properties-post.json')

const describe = mocha.describe
const it = mocha.it

describe('GET /properties', () => {
  it('should get 3 properties', (done) => {
    global.request.get('/properties')
    .end((err, res) => {
      const body = res.body.map((chunk) => {
        const newChunk = deepMerge({}, chunk)

        delete newChunk.id
        delete newChunk.createdAt
        delete newChunk.updatedAt

        return newChunk
      })

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual(propertiesPost, body)

      done()
    })
  })

  it('should fetch an existing property', (done) => {
    global.request.get('/properties/2')
    .end((err, res) => {
      delete res.body.id
      delete res.body.createdAt
      delete res.body.updatedAt

      assert.isNull(err)
      assert.equal(res.status, 200)
      assert.deepEqual(propertiesPost[1], res.body)

      done()
    })
  })

  it('should return 404 when trying to fetch a non existing property', (done) => {
    global.request.get('/properties/1337')
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 404)

      done()
    })
  })
})
