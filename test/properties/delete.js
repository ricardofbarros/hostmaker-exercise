const mocha = require('mocha')
const assert = require('chai').assert

const describe = mocha.describe
const it = mocha.it

describe('DELETE /properties/{id}', () => {
  it('should delete property with id 1', (done) => {
    global.request.delete('/properties/1')
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 200)

      done()
    })
  })

  it('should delete property with id 3', (done) => {
    global.request.delete('/properties/3')
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 200)

      done()
    })
  })

  it('should return 404 if can\'t find the resource to delete', (done) => {
    global.request.delete('/properties/1337')
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 404)

      done()
    })
  })
})
