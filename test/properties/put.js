const mocha = require('mocha')
const assert = require('chai').assert
const deepMerge = require('deepmerge')
const propertiesPost = require('../fixtures/properties-post.json')
const propertiesPut = require('../fixtures/properties-put.json')

const describe = mocha.describe
const it = mocha.it

describe('PUT /properties/{id}', () => {
  it('should update property with id 1', (done) => {
    global.request.put('/properties/1')
    .send(propertiesPut[0])
    .end((err, res) => {
      delete res.body.id
      delete res.body.createdAt
      delete res.body.updatedAt

      assert.isNull(err)
      assert.equal(res.status, 201)
      assert.deepEqual(
        deepMerge(propertiesPost[0], propertiesPut[0]),
        res.body
      )

      done()
    })
  })

  it('should update property with id 2', (done) => {
    global.request.put('/properties/2')
    .send(propertiesPut[1])
    .end((err, res) => {
      delete res.body.id
      delete res.body.createdAt
      delete res.body.updatedAt

      assert.isNull(err)
      assert.equal(res.status, 201)
      assert.deepEqual(
        deepMerge(propertiesPost[1], propertiesPut[1]),
        res.body
      )

      done()
    })
  })

  it('should return 204 if there isn\'t any changes to be made', (done) => {
    global.request.put('/properties/2')
    .send(propertiesPut[1])
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 204)

      done()
    })
  })

  it('should return 404 if can\'t find the resource to update', (done) => {
    global.request.put('/properties/1337')
    .send(propertiesPut[1])
    .end((err, res) => {
      assert.isNull(err)
      assert.equal(res.status, 404)

      done()
    })
  })

  it('should update property with id 2', (done) => {
    global.request.put('/properties/2')
    .send(propertiesPut[2])
    .end((err, res) => {
      delete res.body.id
      delete res.body.createdAt
      delete res.body.updatedAt

      assert.isNull(err)
      assert.equal(res.status, 201)
      assert.deepEqual(
        deepMerge(propertiesPost[1], propertiesPut[2]),
        res.body
      )

      done()
    })
  })
})
