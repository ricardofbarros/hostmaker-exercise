process.env.HTTP_PORT = 8080
process.env.LOGGER_LEVEL = 'fatal'
process.env.DB_NAME = 'hostmaker_test'

const mocha = require('mocha')
const supertest = require('supertest')
const appBoot = require('../src')

const describe = mocha.describe
const before = mocha.before
const after = mocha.after

describe('Setup tests', () => {
  before((done) => {
    appBoot()
    .then(({ app, server }) => {
      const Models = require('../src/models')

      global.request = supertest(app)
      global.server = server

      const truncatePromises = Object.keys(Models).map((name) => Models[name].truncate())

      Promise.all(truncatePromises)
      .then(() => done())
      .catch(done)
    })
    .catch(done)
  })

  after(() => {
    global.server.close()
  })

  // NOTE: Maintain the order of this requires or else you will break the tests
  // this is the testing flow
  require('./properties/post')
  require('./properties/get-after-post')
  require('./properties/put')
  require('./properties/get-histories')
  require('./properties/delete')
  require('./properties/get-after-delete')
})
