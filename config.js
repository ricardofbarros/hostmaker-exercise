const config = {}

config.http = {
  // NOTE: Docker containers when exposed listen on this host
  host: '0.0.0.0',
  port: process.env.HTTP_PORT || 80
}

config.logger = {
  name: 'Hostmaker API',
  level: process.env.LOGGER_LEVEL || 'info'
}

config.db = {
  host: process.env.DB_HOST || 'database',
  user: process.env.DB_USER || 'root',
  pass: process.env.DB_PASS,
  database: process.env.DB_NAME || 'hostmaker',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  logging: process.env.DB_LOG || false
}

// NOTE: Number of proxies to round-robin
// the airbnb requests
config.numOfProxies = 2

module.exports = config
