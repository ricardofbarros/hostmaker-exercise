const Sequelize = require('sequelize')
const database = require('../util/database')()
const airbnbValidator = require('../util/airbnb-validator')

const isSomeUndefined = (props, obj) => props.some((prop) => typeof obj[prop] === 'undefined')

const Properties = database.define('properties', {
  owner: {
    type: Sequelize.STRING,
    allowNull: false
  },
  address: {
    type: Sequelize.JSON,
    validate: {
      test: (propertyAddress) => {
        if (isSomeUndefined(['line1', 'line4', 'postCode', 'city', 'country'], propertyAddress)) {
          throw new Error('property.address validation failed, missing required props')
        }
      }
    },
    allowNull: false
  },
  airbnbId: {
    type: Sequelize.INTEGER,
    validate: {
      test: (id) => airbnbValidator(id)
    },
    allowNull: false
  },
  numberOfBedrooms: {
    type: Sequelize.INTEGER,
    validate: {
      test: (numBeds) => {
        if (numBeds < 0) {
          throw new Error('numberOfBedrooms validation failed')
        }
      }
    },
    allowNull: false
  },
  numberOfBathrooms: {
    type: Sequelize.INTEGER,
    validate: {
      test: (numBaths) => {
        if (numBaths < 1) {
          throw new Error('numberOfBathrooms validation failed')
        }
      }
    },
    allowNull: false
  },
  incomeGenerated: {
    type: Sequelize.FLOAT,
    validate: {
      test: (income) => {
        if (income < 1) {
          throw new Error('incomeGenerated validation failed')
        }
      }
    },
    allowNull: false
  }
})

module.exports = Properties
