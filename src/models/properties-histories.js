const Sequelize = require('sequelize')
const database = require('../util/database')()

// NOTE: We don't need to validate this model because the Properties model
// already have the validations and this model will always be used inside a
// transaction with the Properties model, so we can avoid the double validation overhead
const PropertiesHistories = database.define('properties_histories', {
  propertyId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  owner: {
    type: Sequelize.STRING,
    allowNull: false
  },
  address: {
    type: Sequelize.JSON,
    allowNull: false
  },
  airbnbId: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  numberOfBedrooms: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  numberOfBathrooms: {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  incomeGenerated: {
    type: Sequelize.FLOAT,
    allowNull: false
  }
})

module.exports = PropertiesHistories
