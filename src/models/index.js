const Properties = require('./properties')
const PropertiesHistories = require('./properties-histories')

module.exports = {
  Properties,
  PropertiesHistories
}
