const _ = require('lodash')
const toString = Object.prototype.toString

// NOTE: taken from https://gist.github.com/Yimiprod/7ee176597fef230d1451 and slighty modifed
const difference = (object, base) => {
  return _.transform(object, function (result, value, key) {
    if (!_.isEqual(value, base[key])) {
      result[key] = (_.isObject(value) && _.isObject(base[key])) ? difference(value, base[key]) : value
    }
  })
}

const isNotUndef = (value) => typeof value !== 'undefined'

const cleanArr = (arr) => arr.reduce((accu, chunk, key) => {
  const value = cleanGeneric(key, chunk)

  if (isNotUndef(value)) {
    accu.push(value)
  }

  return accu
}, [])

const cleanObj = (object) => {
  const finalObj = {}

  Object.keys(object).forEach((key) => {
    const value = cleanGeneric(key, object[key])

    if (isNotUndef(value)) {
      finalObj[key] = value
    }
  })

  return finalObj
}

const cleanGeneric = (key, value) => {
  const type = toString.call(value)

  if (type === '[object Object]' && Object.keys(value).length > 0) {
    return cleanObj(value)
  } else if (type === '[object Array]' && value.length > 0) {
    return cleanArr(value)
  } else if (type !== '[object Array]' && type !== '[object Object]') {
    return value
  }
}

function objDiff (object, base) {
  let diff = difference(object, base)

  // NOTE: This diff isn't perfect and can return empty objects and/or arrays
  // we need to clean those
  diff = cleanObj(diff)

  if (Object.keys(diff).length === 0) {
    return null
  }

  return diff
}

module.exports = objDiff
