const config = require('../../config')
const Sequelize = require('sequelize')

let driver

module.exports = () => driver

module.exports.init = () => new Promise((resolve, reject) => {
  const seq = new Sequelize('', config.db.user, config.db.pass, {
    host: config.db.host,
    dialect: config.db.dialect,
    pool: config.db.pool,
    logging: config.db.logging
  })

  const createDriver = () => {
    driver = new Sequelize(config.db.database, config.db.user, config.db.pass, {
      host: config.db.host,
      dialect: config.db.dialect,
      pool: config.db.pool,
      logging: config.db.logging
    })

    resolve()
  }

  // NOTE: It's okay if it errors it means that the database already exsits
  seq.query(`CREATE DATABASE ${config.db.database}`)
  .then(createDriver)
  .catch(createDriver)
})
