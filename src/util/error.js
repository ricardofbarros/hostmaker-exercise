const notFound = (res) => res.status(404).json({
  message: `Resource not found ${res.locals.route}`
})

const controllerResponse = (req, res) => (err) => {
  // NOTE: Model validation errors
  if (Array.isArray(err.errors)) {
    const errors = err.errors.map((error) => {
      return {
        message: error.message,
        type: error.type
      }
    })

    return res.status(422).json({ errors })
  }

  // NOTE: If the program got this far
  // probably, this is an internal server error
  // log the error and return proper response
  req.log.error(err, res.locals.route)
  res.status(500).json({
    message: err.message
  })
}

module.exports = {
  notFound,
  controllerResponse
}
