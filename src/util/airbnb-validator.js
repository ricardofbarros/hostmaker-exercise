const request = require('request-promise-native')
const unprocessableEntity = require('../util/error').unprocessableEntity

const state = {
  proxies: new Set(),
  iterator: null
}

const nextProxy = () => {
  let value

  if (!state.iterator) {
    state.iterator = state.proxies.entries()
    value = state.iterator.next().value
  } else {
    value = state.iterator.next().value

    if (!value) {
      state.iterator = null
      return nextProxy()
    }
  }

  return value[0]
}

const airbnbRequest = (id, resolve, reject) => {
  const proxy = nextProxy()

  request({
    method: 'GET',
    uri: `https://www.airbnb.co.uk/rooms/${id}`,
    headers: {
      'User-Agent': 'request'
    },
    proxy,
    transform: (body, response) => {
      if (!(/^2/.test('' + response.statusCode))) {
        if (response.statusCode === 302) {
          throw unprocessableEntity('Invalid Airbnb ID')
        }

        if (response.statusCode === 503) {
          throw new Error('Airbnb blocked the requests for this proxy')
        }

        throw new Error('Unknown error')
      }

      return true
    },
    followRedirect: false
  })
  .then(resolve)
  .catch((err) => {
    // NOTE: Tunneling could not be established
    // try again with the next proxy
    if (err.message.includes('tunneling socket')) {
      return airbnbRequest(id, resolve, reject)
    }

    // NOTE: Airbnb blocked this proxy
    // try again with the next proxy
    if (err.message.includes('Airbnb blocked the requests')) {
      return airbnbRequest(id, resolve, reject)
    }

    reject(err)
  })
}

function airbnbValidator (id) {
  return new Promise((resolve, reject) => airbnbRequest(id, resolve, reject))
}

function setupProxies () {
  return new Promise((resolve, reject) => {
    state.proxies.add('http://199.195.253.124:3128')
    state.proxies.add('http://134.213.49.60:4444')
    state.proxies.add('http://194.116.198.210:80')
    state.proxies.add('http://134.213.221.177:5800')
    state.proxies.add('http://134.213.148.8:4444')
    state.proxies.add('http://134.213.221.181:4444')
    state.proxies.add('http://134.213.208.193:4444')
    state.proxies.add('http://194.116.198.210:3128')
    state.proxies.add('http://134.213.152.188:3124')
    state.proxies.add('http://134.213.148.8:5007')
    state.proxies.add('http://134.213.148.8:3130')

    resolve()
  })
}

module.exports = airbnbValidator
module.exports.setupProxies = setupProxies
