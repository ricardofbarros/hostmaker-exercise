// NOTE: This util was deprecated in favor to manual setting the proxies
// as the proxies returned by gimmeproxy.com were too slow
const request = require('request-promise-native')
const proxyVerifier = require('proxy-verifier')
const unprocessableEntity = require('../util/error').unprocessableEntity
const config = require('../../config')

const state = {
  proxies: new Set(),
  iterator: null
}

const nextProxy = () => {
  let value

  if (!state.iterator) {
    state.iterator = state.proxies.entries()
    value = state.iterator.next().value
  } else {
    value = state.iterator.next().value

    if (!value) {
      state.iterator = null
      return nextProxy()
    }
  }

  return value[0]
}

function airbnbValidator (id) {
  const proxy = nextProxy()

  return request({
    method: 'GET',
    uri: `https://www.airbnb.co.uk/rooms/${id}`,
    headers: {
      'User-Agent': 'request'
    },
    proxy,
    transform: (body, response) => {
      if (!(/^2/.test('' + response.statusCode))) {
        if (response.statusCode === 302) {
          throw unprocessableEntity('Invalid Airbnb ID')
        }

        if (response.statusCode === 503) {
          throw new Error('Airbnb blocked the requests for this proxy')
        }

        throw new Error('Unknown error')
      }

      return true
    },
    followRedirect: false
  })
}

const getProxy = () => request({
  method: 'GET',
  uri: 'https://gimmeproxy.com/api/getProxy',
  qs: {
    supportsHttps: true,
    protocol: 'http',
    minSpeed: '500',
    anonymityLevel: 1
  },
  json: true
})

// NOTE: This function get the proxies and do an extra verification.
// It verifies if the proxy supports tunneling https requests which is needed to query airbnb
// This verifications must be done because the proxy list provider can return a proxy that doesn't support tunneling
// even if we specify supportsHttps: true
const getProxies = (resolve, reject) => {
  const numOfProxiesMissing = config.numOfProxies - state.proxies.size

  const proxies = []

  for (let i = 0; i < numOfProxiesMissing; i++) {
    proxies.push(getProxy())
  }

  Promise.all(proxies)
  .then((proxiesResults) => {
    const testProxies = proxiesResults.map((result) => testProxy(result))

    Promise.all(testProxies)
    .then((testResults) => {
      testResults.forEach((testResult, index) => {
        if (testResult.ok) {
          state.proxies.add(proxiesResults[index].curl)
        }
      })

      if (state.proxies.size >= config.numOfProxies) {
        resolve()
      } else {
        // NOTE: It still doesn't have the number of proxies needed
        // so it should invoke this function again to fetch new proxies candidates
        getProxies(resolve, reject)
      }
    })
    .catch((err) => reject(err))
  })
}

const testProxy = (proxyData) => new Promise((resolve, reject) => {
  const proxy = {
    protocol: proxyData.protocol,
    ipAddress: proxyData.ip,
    port: proxyData.port
  }
  proxyVerifier.testTunnel(proxy, (err, result) => {
    if (err) {
      return reject(err)
    }

    resolve(result)
  })
})

function setupProxies () {
  return new Promise((resolve, reject) => getProxies(resolve, reject))
}

module.exports = airbnbValidator
module.exports.setupProxies = setupProxies
