const express = require('express')
const { Properties, PropertiesHistories } = require('../models')
const database = require('../util/database')()
const handleBadResponse = require('../util/error').controllerResponse
const notFound = require('../util/error').notFound
const diff = require('../util/obj-diff')
const deepMerge = require('deepmerge')

const app = express()

const plain = true

app.get('/', (req, res) => {
  Properties
  .findAll()
  .then((properties) => {
    res.status(200).json(properties.map((property) => property.get({ plain })))
  })
  .catch(handleBadResponse(req, res))
})

app.post('/', (req, res) => {
  let createdProperty

  database.transaction((transaction) =>
    Properties
    .create(req.body, { transaction })
    .then((property) => {
      const propertyId = property.get('id')

      // NOTE: Set property to a local variable to be returned with the HTTP response
      createdProperty = property.get({ plain })

      return PropertiesHistories.create(Object.assign({ propertyId }, req.body), { transaction })
    })
  )
  .then(() => res.status(201).json(createdProperty))
  .catch(handleBadResponse(req, res))
})

app.get('/:propertyId', (req, res) => {
  Properties
  .findById(req.params.propertyId)
  .then((property) => {
    if (!property) {
      return notFound(res)
    }

    res.status(200).json(property.get({ plain }))
  })
  .catch(handleBadResponse(req, res))
})

app.get('/:propertyId/histories', (req, res) => {
  const propertyId = req.params.propertyId

  PropertiesHistories
  .findAll({
    where: { propertyId }
  })
  .then((properties) => {
    if (properties.length === 0) {
      return notFound(res)
    }

    res.status(200).json(properties.map((property) => property.get({ plain })))
  })
  .catch(handleBadResponse(req, res))
})

app.put('/:propertyId', (req, res) => {
  const propertyId = req.params.propertyId

  Properties
  .findById(req.params.propertyId)
  .then((property) => {
    if (!property) {
      return notFound(res)
    }

    property = property.get({ plain })

    const body = diff(req.body, property)

    // NOTE: There isn't new changes
    // return a No Content status
    if (!body) {
      return res.status(204).end()
    }

    // NOTE: Update property
    property = deepMerge(property, body)

    // NOTE: Create valid property history
    const propertyHistory = Object.assign({}, property, { propertyId })
    delete propertyHistory.id

    database.transaction((transaction) => Promise.all([
      Properties.update(property, { where: { id: propertyId }, transaction }),
      PropertiesHistories.create(propertyHistory, { transaction })
    ]))
    .then((results) => {
      const property = results[1].get({ plain })

      // NOTE: set id and remove propertyId field
      property.id = property.propertyId
      delete property.propertyId

      res.status(201).json(property)
    })
    .catch(handleBadResponse(req, res))
  })
  .catch(handleBadResponse(req, res))
})

app.delete('/:propertyId', (req, res) => {
  const propertyId = req.params.propertyId

  database.transaction((transaction) => {
    return Promise.all([
      Properties.destroy({ where: { id: propertyId }, transaction }),
      PropertiesHistories.destroy({ where: { propertyId }, transaction })
    ])
  })
  .then((result) => {
    if (!result[0]) {
      return notFound(res)
    }

    res.status(200).json({})
  })
  .catch(handleBadResponse(req, res))
})

module.exports = app
