const express = require('express')
const bodyParser = require('body-parser')
const config = require('../config')
const pino = require('express-pino-logger')
const database = require('./util/database')
const setupProxies = require('./util/airbnb-validator').setupProxies

function boot () {
  const app = express()

  return new Promise((resolve, reject) => {
    Promise.all([
      database.init(),
      setupProxies()
    ])
    .then(() => {
      app.use(bodyParser.json())

      // NOTE: Exposes pino logger api
      // on req.log
      app.use(pino(config.logger))

      // NOTE: Log request and
      // store method type and route as a request-level variable
      // this variable is useful to log error + route
      app.use((req, res, next) => {
        res.locals.route = `${req.method} ${req.url}`
        req.log.info(res.locals.route)
        next()
      })

      app.use('/properties', require('./controllers/properties'))

      app.on('error', (err) => console.error('Express app', err))

      // NOTE: Create the tables for all the models defined
      database()
      .sync()
      .then(() => {
        const server = app.listen(config.http.port, config.http.host, () => {
          const { port } = server.address()
          console.log(`API listening at ${port}`)
          resolve({ app, server })
        })
      })
      .catch((err) => {
        console.error('Database sync', err)
        reject(err)
      })
    })
    .catch((err) => {
      console.error('App boot', err)
      reject(err)
    })
  })
}

module.exports = boot
