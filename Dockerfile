FROM node:8.9-alpine

ENV HOME=/home/node/app

COPY ./package.json $HOME/

WORKDIR $HOME

RUN npm install

CMD ["npm", "run", "start"]
