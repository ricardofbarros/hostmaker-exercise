# Prerequisites

- docker >= 17.06.2 ([Installation instructions](https://docs.docker.com/engine/installation/#supported-platforms))
- git
- Optional:
  - node & npm


# Start the application

### With npm
```
git clone git@bitbucket.org:ricardofbarros/hostmaker-exercise.git

cd hostmaker-exercise

npm run setup

## Edit env-vars.example file DB_VOLUME env var to the database-volume directory absolute path
source env-vars.example d

npm run deploy
```

### Without npm
```
git clone git@bitbucket.org:ricardofbarros/hostmaker-exercise.git

cd hostmaker-exercise

mkdir database-volume

chmod 775 database-volume

## Edit env-vars.example file DB_VOLUME env var to the database-volume directory absolute path
source env-vars.example

docker-compose up --build  -d
```

Also, if you are a fan of Postman I’ve included the Postman’s collection with the endpoints. You just need to import `methods.postman_collection` into Postman to start interacting with the API.

# Architectural design
## Tests
You can run the tests with:

`npm run test`

The tests have a coverage report from `nyc`

Also, there is a pre-commit hook, so when someone commits on this project it will run the `lint` and `test` scripts. Preventing commits if `lint` or `test` fails.

## Transactions
This is a must for the bonus history. It guarantees that when a `property` is created a `propertyHistory` is also created. If something goes wrong while creating the property or the `propertyHistory` it rollbacks to the state before those changes

## ORM
I decided to use an ORM (sequelize.js) instead of using a database’s driver package, because with an ORM we get:

- migrations
- seeds
- a simpler API to interact with data
- change the dialect in a heartbeat (postgres, mysql, etc)
- run complex raw queries without overhead, i.e., sends the query directly to the driver

## Validate Airbnb Ids using proxies to avoid blockage

At first when I started implementing the solution I wrote a more automatic solution. It would fetch proxies dynamically through a Proxy list api (gimmeproxy.com) , but it had some problems:
Daily rate limit (about 200)
Some proxies were very slow (taking 10-15 secs to get the response from airbnb)
Had to verify if the proxies supported tunneling https requests, couldn’t trust on the query params of the API to return only proxies that supported HTTPS

I didn’t deleted this solution as it could be interesting to check out. The file is `src/util/airbnb-validator-auto.js`.

The final solution I came with, was manually searching for good proxies that had 100% Uptime and a lower response time and adding them manually in `src/util/airbnb-validator.js`

This solution isn’t perfect because this proxies are still slow, taking an average of 3 secs to get the response from airbnb.
Ideally, the best solution would be to have private proxies instead of public proxies, but since this is an exercise the solution is sufficient

	 

# Thoughts
## Tests

My opinion of unit tests is that they are an anti-pattern (at least on backend Javascript projects), there are similar opinions on the internet (thank god I’m not alone). 

TL;DR Unit tests break easily if not well written. 

Most of the time the unit tests break when large changes or refactors are inserted. This is very painful for the programmer because he will probably take 2x the time of needed to fix the unit tests.


I prefer E2E and integration tests over unit tests, I believe that those tests bring more value to the code base than the latter. E2E tests can break easily as well, but for a different reason. Normally is an unvailable component of the infrastructure, e.g., a database. Luckily, `docker-compose` solves this, as we define all the external dependencies on the `docker-compose.yml`. E2E and integration tests are more resilient to big changes, because in essence it is  blackboxing the service giving it an input and expecting an output, which is very easy to assert with some well written fixtures.
